source = life
output = life
CC = g++
CFLAGS = -O2 -fpermissive

$(output) : $(source).o 
	$(CC) $< -o $(output) 

$(source).o : $(source).cpp 
	$(CC) $(CFLAGS) -c $<

.PHONY : clean
clean : 
	rm -f $(output) $(source).o
