/* Team Project for the Game of Life
   Contributors:
	Pavel Javornik
	David Hallader
	Zishawn Saeed
*/

#include <iostream>
#include <cstdio>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>


using std::cout;
using std::endl;
using std::vector;
using std::string;
using std::ostringstream;
using std::ofstream;

vector<vector<bool> > grid;
bool firstrun = true;
size_t isize, jsize;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Text-Based version of Conway's Game of Life.\n\n"
"   --seed,-s	 FILE	read start state from FILE.\n"
"   --world,-w   FILE	store current world in FILE.\n"
"   --fast-fw,-f NUM	evolve system for NUM generations and quit.\n"
"   --help,-h		show this message and exit.\n";

size_t max_gen = 0; // if > 0, fast forward to this generation
string wfilename = "/tmp/gol-world-current"; // write state here
FILE* fworld = 0; // wfilename handler
string initfilename = "/tmp/gol-world-current"; //read initial state from here
string reset;

/* nbrCount finds number of surrounding cells*/
/* note, when running g++ use the fpermissive option*/
/* i will include that in the make file */
int nbrCount(int ind_i, int ind_j, const vector<vector<bool> >& g) {
	int count = 0;
	int tempx,tempy;
	if(g[ind_j][ind_i]==true)
		--count;
	/* if for loop checks (i,j) then ^ will not count itself */
	for(int x = -1; x<2; ++x)	{
		for(int y = -1; y<2; ++y) {
			tempx = x;
			tempy = y;

			if(ind_j+tempy < 0) //top border case
				tempy = (jsize-1);
			else if(ind_j+tempy == jsize) //bottom border case
				tempy = -(jsize - 1);

			if((ind_i+tempx) < 0) //left border case
				tempx = (isize-1);
			else if(ind_i+tempx == isize) //right border case
				tempx = -(isize-1);

			if(g[ind_j+tempy][ind_i+tempx])
				count++;
		}
	}
	/* iterates through all surrounding grid points, checks if they are
	   boundary points. If they are, then (0,j) or (i,0) or (0,0) is
	   checked instead. */
	//cout<<count<<endl;
	return count;
}
void update() {
	if(firstrun) {
                for(int j = 0; j <jsize; ++j){
                        for(int i = 0; i < isize; ++i) {
                                if(grid[j][i])
                                        cout<<'O';
                                else
                                        cout<<'.';
                        }
                        cout<<'\n';
                }
                firstrun = false; //now this wont run more than once.
        }
        else {
                printf(reset.c_str()); //moves cursor up by ten
                /* way easier with ncurses library. this is a bit of a hax
                   but what can you do? This code won't work on mac or
                   windrones because of that previous printf statement. */
                printf("\r"); //moves cursor to beginning of line (now at 1,1)

		for(int j = 0; j <jsize; ++j){ //overwrites previous image
                        for(int i = 0; i < isize; ++i) {
                                if(grid[j][i]) printf("O");
                                else printf(".");
                        }
                        printf("\n");
                }
        }

}
void dumpState(FILE* fw) {
	//FILE* dump_file = fopen("/tmp/gol-world-current","wb"); no need for his opened in main






}

int initFromFile(const string& fname) { //initial bool vec set up

 fworld = fopen(fname.c_str(), "rb");
  char c;
  vector<bool>vec1;
  while(fread(&c,1,1,fworld)==1){ //stores each character in 1, byte at a time
    switch(c){ //checks what c is
      case '.': vec1.push_back(false); break;
      case 'O': vec1.push_back(true); break;
      case'\n':
        grid.push_back(vec1); // this adds rows to the vertical column of vector
        isize = vec1.size(); //this tells index of the rows
        vec1.clear();//this clears the row vector to be able to put new values in
        break;
    }
  }
  jsize = grid.size();
  fclose(fworld);
  return 1;

}

void mainLoop() {
        /* Creates temporary vector, then overwrites grid according to
           nbrCount. Afterwards it should wait, then update. */
        int surround;

        vector<vector<bool> > vec(jsize,vector<bool>(isize)); // this creates a temporary vector  don't want to change 
grid. import isize jsize from grid
        for(int j = 0; j < jsize; j++) {
                for (int i = 0; i < isize; i++) {
                        surround = nbrCount(i,j,grid); // loops through i,j, and grid. reads grid, but doesn't change 
it. Then we start writing to temporary vector
                        //if(surround < 0) surround = 0;
                        if(grid[j][i] && (surround > 3 || surround < 2)) //over and underpop
                                vec[j][i] = false;
                        else if(grid[j][i] && (surround == 2 || surround == 3)) //survives
                                vec[j][i] = true;
                        else if (!grid[j][i] && surround == 3) //birth
                                vec[j][i] = true;
                }
        }
        sleep(0.2);
        grid = vec; // erases the grid and replaces with the latest generation
}


int main(int argc, char *argv[]) {
	static struct option long_opts[] = {
		{"seed", required_argument, 0, 's'},
		{"world", required_argument, 0, 'w'},
		{"fast-fw", required_argument, 0, 'f'},
		{"help", no_argument, 0, 'h'},
		{0,0,0,0}
	};
	char c;
	int opt_index = 0;
	max_gen = 1;
	while((c = getopt_long(argc, argv, "hs:w:f:", long_opts, &opt_index))!=-1)
	{
		switch(c) {
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case 's':
				initfilename = optarg;
				break;
			case 'w':
				wfilename = optarg;
				break;
			case 'f':
				max_gen = atoi(optarg);
				break;
			case'?':
				printf(usage, argv[0]);
				return 1;
				break;
			default: abort();
		}
	}
	initFromFile(initfilename);

//this may be taken out in final thing
	ostringstream temp;	// <-- Only if running update more than once. buffer to hold characters
	temp<<jsize;
	string val = temp.str();
	reset = "\033["+ val  +"A"; // this takes cursor back to beginning

	//sleep(1);
	fclose(fopen(wfileneame.c_string(),"w")) // to clear the file first interior parens are evaluated.
	fworld = fopen(wfilename.c_str(), "w"); //holds the file in ram fopen must be assigned to file pointer
	if(max_gen > 0) {
		for(int i = 0; i < max_gen; ++i)
			mainLoop();
		}
	else mainLoop(); //main loop is part of else's scope

dumpState(fworld); //rest of this under main runs only once in main.
fclose(fworld);
update();
return 0;
}
