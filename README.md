# README #

## Group Members Contact Info:
- David Hadaller: 
    - email: <dahadall00@citymail.cuny.edu>
    - phone: 845-662-9179
- Pavel Javornik:
    - email: <pjavorn000@citymail.cuny.edu>
    - phone: 917-703-5910

- Zishawn Saeed:
    - email: <zsaeed000@citymail.cuny.edu>
    - phone: 516-695-6805


## Git Resources For This Project
If you have a repo within csc103-lectures with the project folders p1, p2, p3 and you want to make a separate git 
repository just for this Game of Life project in folder p3, this video can show you how. The subject matter covered is 
called "submodules". The instruction starts at 4:29

- [008 Introduction to Git Submodules - YouTube](https://www.youtube.com/watch?v=UQvXst5I41I)

Alternatively, instead of using a submodule, you could just clone this project to any directory that isn't already part 
of a git repository. That's definitely the easier way.
